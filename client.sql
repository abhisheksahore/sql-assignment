CREATE TABLE clients (
    client_id INT NOT NULL PRIMARY KEY ,
    client_name VARCHAR(100),
    client_location VARCHAR(100)
);

CREATE TABLE managers (
    manager_id INT NOT NULL PRIMARY KEY,
    manager_name VARCHAR(100),
    manager_location VARCHAR(100)
);

CREATE TABLE contracts (
    contract_id INT NOT NULL PRIMARY KEY,
    estimation_cost INT NOT NULL,
    completion_date DATETIME NOT NULL,
    FOREIGN KEY (contract_id) REFERENCES clients (client_id) 
);

CREATE TABLE staff (
    staff_id INT NOT NULL PRIMARY KEY,
    staff_name VARCHAR(100),
    staff_location VARCHAR(100)
);

CREATE TABLE clients_managers (
    clients_managers_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    client_id INT NOT NULL,
    manager_id INT NOT NULL,
    FOREIGN KEY (client_id) REFERENCES clients (client_id),
    FOREIGN KEY (manager_id) REFERENCES managers (manager_id)
);

CREATE TABLE managers_staff (
    manager_staff_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    manager_id INT NOT NULL,
    staff_id INT NOT NULL,
    FOREIGN KEY (manager_id) REFERENCES managers (manager_id),
    FOREIGN KEY (staff_id) REFERENCES staff (staff_id)
);
