CREATE TABLE patients (
    patient_id INT NOT NULL PRIMARY KEY,
    patient_name VARCHAR(70),
    patient_dob DATETIME NOT NULL,
    patient_address VARCHAR(255)
);

CREATE TABLE prescriptions (
    prescription_id INT NOT NULL PRIMARY KEY,
    drug VARCHAR(255),
    prescription_date DATETIME NOT NULL,
    dosage VARCHAR(40),
    doctor VARCHAR(70),
    secretary varchar(70)
);

CREATE TABLE patients_prescriptions (
    patients_prescriptions INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    patient_id INT NOT NULL,
    prescription_id INT NOT NULL,
    FOREIGN KEY (patient_id) REFERENCES patients (patient_id),
    FOREIGN KEY (prescription_id) REFERENCES prescriptions (prescription_id)
);
