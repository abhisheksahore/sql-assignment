CREATE TABLE cities (
    pincode INT NOT NULL PRIMARY KEY,
    city VARCHAR(50) 
);

CREATE TABLE publishers (
    publisher_id INT NOT NULL PRIMARY KEY, 
    publisher_name VARCHAR(100)
);

CREATE TABLE branch (
    branch_id INT NOT NULL PRIMARY KEY,
    branch_addr varchar(255),
    pincode INT NOT NULL,
    publisher_id INT NOT NULL,
    FOREIGN KEY (pincode) REFERENCES cities (pincode),
    FOREIGN KEY (publisher_id) REFERENCES publishers (publisher_id)
);

CREATE TABLE books (
    isbn INT NOT NULL PRIMARY KEY UNIQUE,
    title VARCHAR(255),
    num_copies INT ,
    author_name VARCHAR(70),
    publisher_id int not null
);

CREATE TABLE books_branches (
    books_branches_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    book_id INT NOT NULL,
    branch_id INT NOT NULL,
    FOREIGN KEY (book_id) REFERENCES books (Isbn),
    FOREIGN KEY (branch_id) REFERENCES branch (branch_id)
);
