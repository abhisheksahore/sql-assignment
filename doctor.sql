CREATE TABLE doctors (
    doctor_id INT NOT NULL PRIMARY KEY,
    doctor_name VARCHAR(50),
    secretary_name VARCHAR(50)
    
);

CREATE TABLE prescriptions (
    prescription_id INT NOT NULL PRIMARY KEY,
    drug VARCHAR(255),
    prescription_date DATETIME NOT NULL,
    dosage VARCHAR(50)
);

CREATE TABLE patients (
    patient_id INT NOT NULL PRIMARY KEY,
    doctor_id INT NOT NULL,
    patient_name VARCHAR(50) NOT NULL,
    patient_dob DATETIME NOT NULL,
    patient_address VARCHAR(255),
    prescription_id INT NOT NULL,
    FOREIGN KEY (doctor_id) REFERENCES doctors (doctor_id),
    FOREIGN KEY (prescription_id) REFERENCES prescriptions (prescription_id)
);




